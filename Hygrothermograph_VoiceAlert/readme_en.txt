1. This sample program has following features.

	a). Temperature, humidity, atmospheric pressure are periodic measured and display to lcd.

	b). Alert message is notified by voice when temperature, humidity sensors will detect out of range value of the upper or lower.
	   Also, atmospheric pressure sensor observes the progress of 'bomb cyclone', and alert message will be notified by voice when it may be occurred.

	c). Green switch: current measurement result is notified by voice. Also, green led lamp is switched on when it send, and switched off when it speech is completed.

	d). Red switch: red led lamp is switched on when alert function enable, and switched off when it disable.

	e). Notification of the current weather condition, and  alert function enable/disable can work using voice recognition too.

	*When sample program startup, if you add 'ja' option, can transfer japanese text data.
	(you need to install japanese language pack on environment of Haruzira working device using windows 10 settings in this case.)


2. When you will use voice recognition on the network, need to set up of the following in advance using Haruzira.
	a) Remote Devices Page: Add devices informations.
	b) Remote Voice Commands Page: Add commands informations.
	b) User Dictionary Page: Add replace words informations.
	
	*You can take in following sample data file at each page.(It was used on demo video)
	 - Remote Devices data: HzClientDevicesDemo.json
	 - Remote Voice Commands data: HzRemoteVoiceCommandsDemo.json
	 - User Dictionary data: HzUserDictionaryDemo_en.json
	 (note: you need to edit of the devices data, match in your environment.)


3. Operating confirmation environment
	OS: Raspbian version 4.4.32
	Python: version 3.4.2
	Haruzira SDK: version 2.0.0.0
	Haruzira: version 2.1.0.0
	i2c control library: SMBus2 version 0.1.4
	 ¦need to install using pip(pip3) command. (pip3 install smbus2)


4. Source code files.
	* hygrothermograph.py: main program.
	* hz_voice_control.py: haruzira sdk and GPIO control class.
	* lang_resources.py: language resources class.
	* bme280.py: Temperature, humidity, atmospheric pressure sensors control class.
	* lcd_st7032i.py: Lcd module control class.
	
	
5. Main electronic parts.
	* AE-BME280(Akizuki denshi):
				  - using BOSH BME280.
				  - DC1.71V - 3.6V
				  - I2C, SPI
				  - measurement range and precision:
				  		temperature: -40 - +85, }1
				  		humidity: 0 - 100%, }3%
				  		atmospheric pressure: 300 - 1100hPa, }1hPa
				  - resolution:
				  		temperature: 0.1
				  		humidity: 0.008%
				  		atmospheric pressure: 0.18Pa
				  	http://akizukidenshi.com/catalog/g/gK-09421/
		
	* LCD module(Strawberry Linux):
				  - 16 characters * 2 lines and icons(not use).
				  - 3.3V, I2C connect.
				  - HD44780 compatible.
				  - ST7032i controller installed.
				  - https://strawberry-linux.com/catalog/items?code=27001
	
	

Also, please refer to SDK user's manual too.
