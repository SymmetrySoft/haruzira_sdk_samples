#coding: utf-8

from smbus2 import SMBus
import time

#BME280 control class
class Bme280(object):
    def __init__(self):
        self.__i2c_address = 0x76

        self.__bus = SMBus(1)   #1: bus number

        self.__digT = []    #temperature
        self.__digP = []    #pressure
        self.__digH = []    #humidity

        self.__t_fine = 0.0


    def dispose(self):
        self.__bus.close()

    def __set_calib(self):
        calib = []
        
        #calibration data(0x88...0xA1)
        for i in range (0x88,0x88+24):
            calib.append(self.__bus.read_byte_data(self.__i2c_address,i))
        calib.append(self.__bus.read_byte_data(self.__i2c_address,0xa1))

        #calibration data(0xE1...0xF0)
        for i in range (0xe1,0xe1+7):
            calib.append(self.__bus.read_byte_data(self.__i2c_address,i))

        self.__digT.append((calib[1] << 8) | calib[0])
        self.__digT.append((calib[3] << 8) | calib[2])
        self.__digT.append((calib[5] << 8) | calib[4])
        self.__digP.append((calib[7] << 8) | calib[6])
        self.__digP.append((calib[9] << 8) | calib[8])
        self.__digP.append((calib[11]<< 8) | calib[10])
        self.__digP.append((calib[13]<< 8) | calib[12])
        self.__digP.append((calib[15]<< 8) | calib[14])
        self.__digP.append((calib[17]<< 8) | calib[16])
        self.__digP.append((calib[19]<< 8) | calib[18])
        self.__digP.append((calib[21]<< 8) | calib[20])
        self.__digP.append((calib[23]<< 8) | calib[22])
        self.__digH.append( calib[24] )
        self.__digH.append((calib[26]<< 8) | calib[25])
        self.__digH.append( calib[27] )
        self.__digH.append((calib[28]<< 4) | (0x0f & calib[29]))
        self.__digH.append((calib[30]<< 4) | ((calib[29] >> 4) & 0x0f))
        self.__digH.append( calib[31] )
        
        #temperature
        for i in range(1,2):
            if(self.__digT[i] & 0x8000):
                self.__digT[i] = (-self.__digT[i] ^ 0xffff) + 1

        #pressure
        for i in range(1,8):
            if(self.__digP[i] & 0x8000):
                self.__digP[i] = (-self.__digP[i] ^ 0xffff) + 1

        #humidity
        for i in range(0,6):
            if(self.__digH[i] & 0x8000):
                self.__digH[i] = (-self.__digH[i] ^ 0xffff) + 1  

    #en:read data of the temperature, humidity, pressure.
    #ja:温度・湿度・気圧データの読み込み
    def read_data(self):
        data = []
        #read from register address(0xF7...0xFE)
        for i in range (0xf7, 0xf7+8):
            data.append(self.__bus.read_byte_data(self.__i2c_address,i))
        pres_data = (data[0] << 12) | (data[1] << 4) | (data[2] >> 4)
        temp_data = (data[3] << 12) | (data[4] << 4) | (data[5] >> 4)
        hum_data  = (data[6] << 8)  |  data[7]
        
        return (self.get_temperature(temp_data), self.get_press(pres_data), self.get_humidity(hum_data))

    #en:get pressure value and calc 
    #ja:気圧の取得と計算
    def get_press(self, adc_P):
        self.__t_fine
        pressure = 0.0
        
        v1 = (self.__t_fine / 2.0) - 64000.0
        v2 = (((v1 / 4.0) * (v1 / 4.0)) / 2048) * self.__digP[5]
        v2 = v2 + ((v1 * self.__digP[4]) * 2.0)
        v2 = (v2 / 4.0) + (self.__digP[3] * 65536.0)
        v1 = (((self.__digP[2] * (((v1 / 4.0) * (v1 / 4.0)) / 8192)) / 8)  + ((self.__digP[1] * v1) / 2.0)) / 262144
        v1 = ((32768 + v1) * self.__digP[0]) / 32768
        
        if(v1 == 0):
            return 0
        pressure = ((1048576 - adc_P) - (v2 / 4096)) * 3125
        if(pressure < 0x80000000):
            pressure = (pressure * 2.0) / v1
        else:
            pressure = (pressure / v1) * 2
        v1 = (self.__digP[8] * (((pressure / 8.0) * (pressure / 8.0)) / 8192.0)) / 4096
        v2 = ((pressure / 4.0) * self.__digP[7]) / 8192.0
        pressure = pressure + ((v1 + v2 + self.__digP[6]) / 16.0)  

        print("pressure: %.2f hPa" % (pressure / 100))
        return pressure / 100

    #en:get temperature value and calc 
    #ja:温度の取得と計算
    def get_temperature(self, adc_T):
        self.__t_fine
        v1 = (adc_T / 16384.0 - self.__digT[0] / 1024.0) * self.__digT[1]
        v2 = (adc_T / 131072.0 - self.__digT[0] / 8192.0) * (adc_T / 131072.0 - self.__digT[0] / 8192.0) * self.__digT[2]
        self.__t_fine = v1 + v2
        temperature = self.__t_fine / 5120.0
        print("temp: %.2f ℃" % (temperature) )
        return temperature
    
    #en:get humidity value and calc 
    #ja:湿度の取得と計算
    def get_humidity(self, adc_H):
        self.__t_fine
        var_h = self.__t_fine - 76800.0
        if(var_h != 0):
            var_h = (adc_H - (self.__digH[3] * 64.0 + self.__digH[4]/16384.0 * var_h)) * (self.__digH[1] / 65536.0 * (1.0 + self.__digH[5] / 67108864.0 * var_h * (1.0 + self.__digH[2] / 67108864.0 * var_h)))
        else:
            return 0
        var_h = var_h * (1.0 - self.__digH[0] * var_h / 524288.0)
        if(var_h > 100.0):
            var_h = 100.0
        elif var_h < 0.0:
            var_h = 0.0
        print("hum: %.2f ％" % (var_h))
        return var_h


    #en:setup register.
    #ja:レジスタ領域の設定        
    def setup(self):
        #en:refer to memory map for BME280 register.
        #ja:アドレスや設定項目の詳細はBME280レジスタのメモリーマップやデータシートを参照する。
        osrs_t = 1          #Temperature oversampling x 1
        osrs_p = 1          #Pressure oversampling x 1
        osrs_h = 1          #Humidity oversampling x 1
        mode   = 3          #Normal mode
        t_sb   = 5          #Tstandby 1000ms
        filter = 0          #Filter off
        spi3w_en = 0            #3-wire SPI Disable

        ctrl_meas_reg = (osrs_t << 5) | (osrs_p << 2) | mode
        config_reg    = (t_sb << 5) | (filter << 2) | spi3w_en
        ctrl_hum_reg  = osrs_h

        #write to register
        self.__bus.write_byte_data(self.__i2c_address, 0xf2, ctrl_hum_reg)
        self.__bus.write_byte_data(self.__i2c_address, 0xf4, ctrl_meas_reg)
        self.__bus.write_byte_data(self.__i2c_address, 0xf5, config_reg)

        self.__set_calib()
