# coding: utf-8
import threading

#Languge resources class.(singleton class)
class LangResources(object):
    __strings_en = {"alert_on":"Yes, certainly.\r\nI will enable an alert.", \
                "alert_off":"Yes, certainly.\r\nI will disable an alert.", \
                "weather_condition":"Yes, certainly.\r\nI will do it right now.", \
                "invalid_command":"Invalid command data received.\r\nYour command '{}' can not any work.", \
                "weather_time":"I announce about the weather condition as of {0:02d}:{1:02d}:{2:02d}.", \
                "weather_value":"%s\r\nTemperature: %s\r\nHumidity: %s\r\nPressure: %s\r\n", \
                "temp_min":"The temperature is being in low than your set point.\r\nYou may want to be careful of the cold.", \
                "temp_max":"The temperature is being over than your set point.\r\nYou may as well be careful of heatstroke.", \
                "hum_min":"The humidity is being in low and dry.\r\nIt would be careful don't catch a cold, for the flu virus may be more active.", \
                "hum_max":"The humidity is being in high and may be feeling is not better.\r\nIt would be careful the mites and mold.", \
                "bomb_cyclone":"May be, 'Bomb Cyclone' is occurring.\r\nI think you had better be careful of change in the weather condition.", \
                }

    __strings_ja = {"alert_on":"了解しました。\r\nアラートを有効にします。", \
                "alert_off":"了解しました。\r\nアラートを無効にします。", \
                "weather_condition":"Yes, certainly.\r\nI will do it right now.", \
                "invalid_command":"不明なコマンドを受信しました。コマンド「{}」に対する処理は実行できません。", \
                "weather_time":"{0:02d}時{1:02d}分{2:02d}秒、現時点での気象状況をお知らせいたします。", \
                "weather_value":"%s\r\n気温： %s\r\n湿度： %s\r\n気圧： %s\r\n", \
                "temp_min":"気温が設定温度を下回っています。\r\n寒さによる、体温の低下に気を付けて下さい。", \
                "temp_max":"気温が設定温度を超えました。\r\n熱中症に注意して下さい。", \
                "hum_min":"湿度が低く空気が乾燥しています。\r\n肌や喉が乾燥しやすい状態です。インフルエンザウィルスが活発になりやすいので、カゼなどに注意して下さい。", \
                "hum_max":"湿度が高く快適な状態ではありません。\r\nカビ、ダニが発生しやすいので注意して下さい。", \
                "bomb_cyclone":"爆弾低気圧が発生している可能性があります。\r\n今後の天候の変化には、十分に注意して下さい。", \
                }

    __langid_tbl = {"en":0x0409, "ja":0x0411}

    _instance = None
    _lock = threading.Lock()

    def __init__(self, argvs):
        pass

    def __new__(cls, argvs):
        with cls._lock:
            if(cls._instance is None):
                cls._instance = super().__new__(cls)
                cls.__strings = cls.__strings_en
                cls.__langid = cls.__langid_tbl["en"]

                # en:get command parameter to set language code
                # ja:言語設定を行うためのパラメータ取得
                if(len(argvs) > 1):
                    if(argvs[1].lower() == "ja"):
                        cls.__strings = cls.__strings_ja
                        cls.__langid = cls.__langid_tbl["ja"]
                    else:
                        cls.__strings = cls.__strings_en
                        cls.__langid = cls.__langid_tbl["en"]
                else:
                    cls.__strings = cls.__strings_en
                    cls.__langid = cls.__langid_tbl["en"]


        return cls._instance

    #region define accessor
    def __get_strings(self):
        return self.__strings

    def __set_strings(self, value):
        self.__strings = value

    def __get_langid(self):
        return self.__langid

    def __set_langid(self, value):
        self.__langid = value
    #endregion

    #region Property
    strings = property(__get_strings, __set_strings)
    langid = property(__get_langid, __set_langid)
    #endregion
