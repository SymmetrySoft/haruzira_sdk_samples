# coding: utf-8
#This program will be ended when enter 'Ctrl + C'.
import RPi.GPIO as GPIO
import sys
import time
import threading
import datetime
import math
from enum import Enum
from haruzirasdk import hz_client_tcp_communication as hzcomm
import lang_resources

#region enum
class Bme280MeasurementType(Enum):
    Period = 0x00      #periodic measurement
    User = 0x01        #timing by user
#endregion

SERVER_IP = "192.168.1.6"
SERVER_PORT = 46000
MIN = 0
MAX = 1
#need to convert degree to radian
BOMB_CYCLONE = 24 * (math.sin(math.radians(35)) / math.sin(math.radians(60))) #24hPa * (sin(latitude) / sin(60°))   #latitude 35: around Tokyo 
TEMPERATURE_LIMIT = (10.00, 30.00) #(min, max)
HUMIDITY_LIMIT = (40.00, 70.00) #(min, max)

#Voice communication and GPIO control class
class HzVoiceControl(object):

    __GPIO_IN_TACT_ALERT = 22
    __GPIO_IN_TACT_WEATHER = 23
    __GPIO_OUT_LED_GREEN = 24
    __GPIO_OUT_LED_RED = 25

    # <summary>
    # Initialize
    # </summary>
    # <param name="args">parameters for language code</param>
    # <param name="tcp_comm">ClientTcpCommunication class instance</param>
    # <param name="callback1">callback function when green tact switch rising, 'weather condition' command receive</param>
    # <returns>True:OK, False:NG</returns>
    def __init__(self, argvs, tcp_comm, bme, lcd):
        self.__led_state_green = GPIO.LOW   #Green LED
        self.__led_state_red = GPIO.HIGH   #Red LED
        self.__alert_led_blink = False  #Red LED blinking
        self.__clt_tcp_comm = tcp_comm  #haruzirasdk instance
        self.__bme = bme                #Bme280 instance
        self.__lcd = lcd                #LcdSt7032i instance
        self.__gpio_worker = None
        self.__gpio_worker_break = False    #False: continue worker thread, True: break worker thread
        self.__lock_alert_gpio = threading.Lock()
        self.__send_weather_time = None     #timestamp when weather condition message send
        self.__send_alert_on_time = None    #timestamp when alert on responce message send.(after alert on command received)
        self.__send_alert_off_time = None   #timestamp when alert off responce message send.(after alert off command received)
        self.__send_alert_time = None       #timestamp when alert message send.
        self.__pressure_records = []        #pressure measurement records for 'Bomb Cyclone'. (Max 24 items)
        self.__start_pres_measurement = 0   #time when pressure measurement start
        #self.__evNotifyWeatherCondition = callback1     #callback function when tact switch rising, 'weather condition' command receive
        #self.__evNotifyAlertEnable = callback2     #callback function when tact switch rising(alert)


        #Initialize GPIO. 
        GPIO.setmode(GPIO.BCM) #BCM:GPIO No, BOARD:BOARD No
        #GPIO.setwarnings(False)
        GPIO.setup(self.__GPIO_OUT_LED_GREEN, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.__GPIO_OUT_LED_RED, GPIO.OUT, initial=GPIO.HIGH)
        #It use pull-down resistor on the raspberry pi.
        GPIO.setup(self.__GPIO_IN_TACT_ALERT, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.__GPIO_IN_TACT_WEATHER, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        #Register callback function.(occurred when detected rising event)
        #bouncetime is needed for clearing noise of positive edge event.
        GPIO.add_event_detect(self.__GPIO_IN_TACT_ALERT, GPIO.RISING, callback=self.__rising_tact_alert_callback, bouncetime=500)
        GPIO.add_event_detect(self.__GPIO_IN_TACT_WEATHER, GPIO.RISING, callback=self.__rising_tact_weather_callback, bouncetime=500)

        # en:get command parameter to set language code
        # ja:言語設定を行うためのパラメータ取得
        self.__lrs = lang_resources.LangResources(argvs)

        #Initialize haruzira sdk class. and register callback function.
        self.__clt_tcp_comm.evNotifyCompleteSpeech = self.__rcv_notify_complete_speech
        self.__clt_tcp_comm.evNotifyMessageEvent = self.__notify_communicaton_message
        self.__clt_tcp_comm.evNotifyReceivedDisconnectEvent = self.__notify_received_disconnect
        self.__clt_tcp_comm.evNotifySendSpeechRecognitionCommand = self.__rcv_send_speech_recognition_command
        self.__clt_tcp_comm.setTraceOutPut(False)

        #start worker thread
        self.__gpio_worker = threading.Thread(target=self.__gpio_worker_proc)
        self.__gpio_worker.start() 


    ###############################################################
    # en:call back functions for haruzirasdk. called when occurred events on a network communication.
    # ja:通信イベントコールバック関数定義
    ###############################################################
    # <summary>
    # en:notify when occurred issue that disconnected from access point or on receiving threads.
    # ja:切断発生時通知イベント（サーバー側からの切断または受信スレッドで異常発生時）
    # </summary>
    # <param name="msg">message</param>
    # <param name="st">status</param>
    def __notify_received_disconnect(self, msg, st):
        print("{0:s}, Status[0x{1:02x}]".format(msg, st))

    # <summary>
    # en:notify when occurred issue for some reasons(except disconnect).
    # ja:通信障害（切断以外）発生時通知イベント
    # </summary>
    # <param name="msg">message</param>
    # <param name="msg_id">message id</param>
    # <param name="err_code">error code</param>
    def __notify_communicaton_message(self, msg, msg_id, err_code):
        print("Message ID[0x{0:02x}], Error Code[0x{1:02x}], {2:s}".format(msg_id, err_code, msg))

    # <summary>
    # en:notify when received a 'speech completion notice' message.
    # ja:読み上げ完了通知受信時通知イベント
    # </summary>
    # <param name="result">submit result</param>
    # <param name="time_stamp">time stamp</param>
    def __rcv_notify_complete_speech(self, result, time_stamp):
        #print("{0:s}：result[0x{1:02x}], time stamp[{2:s}]".format(self.__strings["rec_comp"], result, time_stamp))
        print("recieved notify complete speech. time stamp[{0:s}]".format(time_stamp))
        if(self.__led_state_green == GPIO.HIGH and time_stamp == self.__send_weather_time):
            #green led off
            GPIO.output(self.__GPIO_OUT_LED_GREEN, GPIO.LOW)
            self.__led_state_green = GPIO.LOW
            self.__send_weather_time = None
        #if(self.__led_state_red == GPIO.HIGH and time_stamp == self.__send_alert_on_time):
        if(time_stamp == self.__send_alert_on_time):
            #red led on
            GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.HIGH)
            self.__led_state_red = GPIO.HIGH
            self.__send_alert_on_time = None
        #elif(self.__led_state_red == GPIO.LOW and time_stamp == self.__send_alert_off_time):
        elif(time_stamp == self.__send_alert_off_time):
            #red led off
            GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.LOW)
            self.__led_state_red = GPIO.LOW
            self.__send_alert_off_time = None
        
        if(time_stamp == self.__send_alert_time):
            #send alert messsage
            self.__send_alert_time = None

    # <summary>
    # en:notify when receive a 'send speech recognition command' message.
    # ja:音声認識コマンド送信メッセージ受信時イベント
    # </summary>
    # <param name="cmdInfo">HzSpeechRecognitionCommandInfo class object</param>
    def __rcv_send_speech_recognition_command(self, cmdInfo):
        try:
            print("{0:s} : Ip[{1:s}], Port[{2:d}], Mode[{3:d}], Command[{4:s}], Timestamp[{5:s}]".format("Received, 'Send Speech Recognition Command' message.", 
                cmdInfo.ip_addr, cmdInfo.port, cmdInfo.mode, cmdInfo.command, cmdInfo.timestamp))
            #return a message to sender.
            self.__clt_tcp_comm.ServerPortNo = cmdInfo.port
            self.__clt_tcp_comm.ServerIP = cmdInfo.ip_addr
            #comp_evt.clear()
            if(cmdInfo.command.lower() == "weather condition"):
                GPIO.output(self.__GPIO_OUT_LED_GREEN, GPIO.HIGH)
                self.__led_state_green = GPIO.HIGH
                self.__send_weather_time = self.__notify_weather_condition(cmdInfo.ip_addr, cmdInfo.port)
            elif(cmdInfo.command.lower() == "alert on"):
                #self.__led_state_red = GPIO.HIGH
                msg = self.__lrs.strings["alert_on"]
                self.__send_alert_on_time = self.__notify_alert_enable(msg, cmdInfo.ip_addr, cmdInfo.port)
            elif(cmdInfo.command.lower() == "alert off"):
                #self.__led_state_red = GPIO.LOW
                msg = self.__lrs.strings["alert_off"]
                self.__send_alert_off_time = self.__notify_alert_enable(msg, cmdInfo.ip_addr, cmdInfo.port)
            else:
                msg = self.__lrs.strings["invalid_command"].format(cmdInfo.command)
            
        except Exception as ex:
            print(ex.args)
    #end define call back function for haruzira

    #region callback function.(occurred when detected rising event)
    #This function is used by tact switch. (Alert)
    #ja:タクトスイッチのイベント検出用（アラート）
    def __rising_tact_alert_callback(self, channel):
        if(channel == self.__GPIO_IN_TACT_ALERT):
            if(self.__led_state_red == GPIO.LOW):
                GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.HIGH)
                self.__led_state_red = GPIO.HIGH
            else:
                GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.LOW)
                self.__led_state_red = GPIO.LOW

    #This function is used by tact switch. (Speech weater condition)
    #ja:タクトスイッチのイベント検出用（気象状況）
    def __rising_tact_weather_callback(self, channel):
        if(channel == self.__GPIO_IN_TACT_WEATHER):
            if(self.__led_state_green == GPIO.LOW):
                GPIO.output(self.__GPIO_OUT_LED_GREEN, GPIO.HIGH)
                self.__led_state_green = GPIO.HIGH
                self.__send_weather_time = self.__notify_weather_condition()
                if(self.__send_weather_time is None):
                    #sending failed
                    GPIO.output(self.__GPIO_OUT_LED_GREEN, GPIO.LOW)
                    self.__led_state_green = GPIO.LOW
    #endregion
    

    #en:make text data for sending to access point.
    #ja:送信用テキストの作成
    def __make_message(self, type, msg, temp, pres, hum):
        r_msg = ""
        if(type == Bme280MeasurementType.User.value):
            r_msg = msg

        if(temp < TEMPERATURE_LIMIT[MIN]):
            r_msg += "\r\n" + self.__lrs.strings["temp_min"]
        elif(temp > TEMPERATURE_LIMIT[MAX]):
            r_msg += "\r\n" + self.__lrs.strings["temp_max"]
        if(hum < HUMIDITY_LIMIT[MIN]):
            r_msg += "\r\n" + self.__lrs.strings["hum_min"]
        elif(hum > HUMIDITY_LIMIT[MAX]):
            r_msg += "\r\n" + self.__lrs.strings["hum_max"]
        if(self.__check_bomb_cyclone(pres)):
            r_msg += "\r\n" + self.__lrs.strings["bomb_cyclone"]

        return r_msg

    def __set_accesspoint_info(self, ip, port):
        if(ip == None):
            self.__clt_tcp_comm.ServerPortNo = SERVER_PORT #port number for access point
            self.__clt_tcp_comm.ServerIP = SERVER_IP #ip address for access point
        else:
            self.__clt_tcp_comm.ServerPortNo = port #port number for access point
            self.__clt_tcp_comm.ServerIP = ip #ip address for access point

    #en:occurred when 'weather condition' command receive, or tact switch event detect.
    #ja:「weather condition」コマンド受信時またはタクトスイッチイベント発生時
    def __notify_weather_condition(self, ip = None, port = None):
        #text to speech data send to access point
        msg, temp, pres, hum = self.__get_hygrothemograph_valeus()
        r_msg = self.__make_message(Bme280MeasurementType.User.value, msg, temp, pres, hum)
        self.__set_accesspoint_info(ip, port)
        self.__clt_tcp_comm.ReqSendDataText = r_msg
        self.__clt_tcp_comm.ReqSendDataSpeechLevel = hzcomm.HzSpeechLevel.Normal.value
        self.__clt_tcp_comm.ReqSendDataSpeechLocaleId = self.__lrs.langid
        self.__clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Female.value
        self.__clt_tcp_comm.ReqSendDataSpeechRepeat = 0
        return self.__clt_tcp_comm.sendSpeechDataEx()

    #en:occurred when 'alert on/off' command receive, or tact switch event detect.
    #ja:「alert on/off」コマンド受信時またはタクトスイッチイベント発生時
    def __notify_alert_enable(self, st, ip = None, port = None):
        #text to speech data send to access point
        self.__set_accesspoint_info(ip, port)
        self.__clt_tcp_comm.ReqSendDataText = st
        self.__clt_tcp_comm.ReqSendDataSpeechLevel = hzcomm.HzSpeechLevel.High.value
        self.__clt_tcp_comm.ReqSendDataSpeechLocaleId = self.__lrs.langid
        self.__clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Female.value
        self.__clt_tcp_comm.ReqSendDataSpeechRepeat = 0
        return self.__clt_tcp_comm.sendSpeechDataEx()

    #en:get temperature, humidity, pressure from each sensor, and write to lcd.
    #ja:温度・湿度・気圧センサーから値を取得しLCDへ書き込む
    def __get_hygrothemograph_valeus(self):
        temp, pres, hum = self.__bme.read_data()
        s_temp = "%.1f℃ " % temp
        s_temp2 = "%.1f\337C  " % temp
        s_hum = "%.2f%s" % (hum, "%")
        s_pres = "%.2f hPa" % pres
        t = datetime.datetime.today()
        timeStamp = self.__lrs.strings["weather_time"].format(t.hour, t.minute, t.second)
        msg = self.__lrs.strings["weather_value"] % (timeStamp, s_temp, s_hum, s_pres)
        print(msg)

        #write data to lcd
        self.__lcd.clear_display()
        #self.__lcd.write_string(s_temp2 + s_hum)
        #self.__lcd.write_string(s_pres)
        self.__lcd.write_line_data(self.__lcd.LINE1, s_temp2 + s_hum)
        self.__lcd.write_line_data(self.__lcd.LINE2, s_pres)

        return msg, temp, pres, hum

    #en:check 'Bomb Cyclone'
    #ja:爆弾低気圧のチェック
    def __check_bomb_cyclone(self, pres):
        #interval time 1h. (max 24h)
        ret = False
        if(self.__start_pres_measurement == 0 or time.time() - self.__start_pres_measurement >= 3600):
            #pressure value record and start time refresh
            if(len(self.__pressure_records) < 24):
                self.__pressure_records.append(pres)
            else:
                del self.__pressure_records[0]
                self.__pressure_records.append(pres)
            self.__start_pres_measurement = time.time()
            #check
            if(len(self.__pressure_records) > 1):
                min_v = min(self.__pressure_records[1:len(self.__pressure_records)])
                if(self.__pressure_records[0] - min_v >= BOMB_CYCLONE):
                    #alert bomb cyclone
                    print("{} - {} = {}".format(self.__pressure_records[0], min_v, self.__pressure_records[0] - min_v))
                    ret = True

            #print(self.__pressure_records)
        return ret

    #region public method
    #dispose class object
    def gpio_dispose(self):
        GPIO.cleanup()
        self.__gpio_worker_break = True
        if(self.__gpio_worker != None and self.__gpio_worker.is_alive()):
            self.__gpio_worker.join(3)
            self.__gpio_worker = None


    #en:periodic measurement
    #ja:周期的な計測
    def periodic_measurement(self):
        msg, temp, pres, hum = self.__get_hygrothemograph_valeus()
        r_msg = self.__make_message(Bme280MeasurementType.Period.value, msg, temp, pres, hum)
        if(r_msg != ""):
            self.__alert_led_blink = True
            if(self.__led_state_red == GPIO.HIGH and self.__send_alert_time is None):
                #send alert message when alert led enable.
                self.__clt_tcp_comm.ServerPortNo = SERVER_PORT #port number for access point
                self.__clt_tcp_comm.ServerIP = SERVER_IP #ip address for access point
                self.__clt_tcp_comm.ReqSendDataText = r_msg
                self.__clt_tcp_comm.ReqSendDataSpeechLevel = hzcomm.HzSpeechLevel.High.value
                self.__clt_tcp_comm.ReqSendDataSpeechLocaleId = self.__lrs.langid
                self.__clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Female.value
                self.__clt_tcp_comm.ReqSendDataSpeechRepeat = 1    #repeat 2 times
                self.__send_alert_time = self.__clt_tcp_comm.sendSpeechDataEx()
        else:
            #GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.LOW)
            self.__alert_led_blink = False
                
 
    #endregion



    #en:alert led control thread
    #ja:アラートLED制御スレッド
    def __gpio_worker_proc(self):
        #Loop until when be interrupted by 'Ctrl + C'.
        try:
            while True:
                if(self.__alert_led_blink and self.__led_state_red == GPIO.HIGH):
                    #red 'led' blinking when alert detected
                    with self.__lock_alert_gpio:
                        GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.HIGH)
                        time.sleep(0.3)
                        GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.LOW)
                        time.sleep(0.3)
                elif(self.__alert_led_blink == False and self.__led_state_red == GPIO.HIGH):
                    with self.__lock_alert_gpio:
                        if(GPIO.input(self.__GPIO_OUT_LED_RED) == GPIO.LOW):
                            #led lamp on
                            GPIO.output(self.__GPIO_OUT_LED_RED, GPIO.HIGH)
                        time.sleep(0.1)
                else:
                    time.sleep(0.1)
                
                if(self.__gpio_worker_break):
                    break
        
        except KeyboardInterrupt:
            pass
        except Exception as ex:
            pass
            #print(ex.args)
        finally:
            pass
