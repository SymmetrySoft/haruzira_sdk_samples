# -*- coding: utf-8 -*-
from smbus2 import SMBus
import sys
from time import sleep

#LCD control class
class LcdSt7032i(object):
    LINE1 = 1
    LINE2 = 2

    def __init__(self):
        self.__bus = SMBus(1)   #i2C bus: 1
        self.__address_st7032i = 0x3e   #LCD module address is fixed value of the '0b0111110'.(0x3e) (ja: アドレスは固定：0b0111110)
        self.__address_command = 0x00 #For command address. (ja: 制御コマンドアドレス)
        self.__address_data = 0x40 #For data write address. (ja: データ書き込みアドレス。)

        self.__contrast = 42 # Contrast value range.(0 - 63) Recommended value is about range of 30 - 42. (ja: コントラストの範囲(0-63)。30から42程度を推奨)
        self.__line_max_chars = 16 # Character total number on a line. (ja:LCDの１行に対する横方向の文字数)
        self.__display_lines = 2   # LCD line numbers. (ja:LCDの行数)

        self.__display_max_chars = self.__line_max_chars * self.__display_lines

        self.__current_pos = 0
        self.__current_line = 0


    #en:initialize lcd module
    #ja:LCDモジュールの初期化
    def init_st7032i(self):
        trials = 5
        for i in range(trials):
            try:
                #Initialize sequence flow(refer to data sheet - i2c-lcd-an001.pdf). (ja: データシートの初期化手順フローを参照 - i2c-lcd-an001.pdf)
                c_lower = (0x70 | (self.__contrast & 0xf))       #contrast low
                c_upper = (0x54 | (self.__contrast >> 4) & 0x3)  #contrast High/Icon/Power
                #0x38: function set, 0x39: function set, 0x14: interval osc frequency, c_lower: contrast low, c_upper: contrast High/Icon/Power, 0x6c: follower control
                self.__bus.write_i2c_block_data(self.__address_st7032i, self.__address_command, [0x38, 0x39, 0x14, c_lower, c_upper, 0x6c])
                sleep(0.2)
                ##0x38: function set, 0x0c: Display On/Off control, 0x01: Clear Display 
                self.__bus.write_i2c_block_data(self.__address_st7032i, self.__address_command, [0x38, 0x0c, 0x01])
                sleep(0.001)    #need to additional wait time for clearing display.
                break
            except IOError:
                if(i == trials - 1):
                    sys.exit()

    #clear display
    def clear_display(self):
        self.__current_pos = 0
        self.__current_line = 0
        #0x01:clear display command
        self.__bus.write_byte_data(self.__address_st7032i, self.__address_command, 0x01)
        sleep(0.001)

    #line feed
    def newline(self):
        if(self.__current_line == self.__display_lines - 1):
            self.clear_display()
        else:
            self.__current_line += 1
            self.__current_pos = self.__line_max_chars * self.__current_line
            self.__bus.write_byte_data(self.__address_st7032i, self.__address_command, 0xc0)
            sleep(0.001)
    
    #en:in case of writing string of the over 17 characters to lcd
    #ja:17文字以上の文字列の書き込み時に利用する
    def write_string(self, s):
        for c in list(s):
            self.write_char(ord(c))

    #en:write a character
    #ja:１文字毎に書き込み
    def write_char(self, c):
        byte_data = self.__check_writable(c)
        if(self.__current_pos == self.__display_max_chars):
            self.clear_display()
        elif(self.__current_pos == self.__line_max_chars * (self.__current_line + 1)):
            self.newline()
        self.__bus.write_byte_data(self.__address_st7032i, self.__address_data, byte_data)
        self.__current_pos += 1

    # <summary>
    # en:write to data in the line.
    # ja:指定行に文字列を書き込む
    # </summary>
    # <param name="lnum">line number(1 or 2)</param>
    # <param name="s">write string data</param>
    def write_line_data(self, lnum, s):
        if(lnum == self.LINE2):
            #en:line feed(move to second line) - 0xc0: line feed command
            #ja:２行目の場合は改行する。(0xc0: 改行コマンド)
            self.__bus.write_byte_data(self.__address_st7032i, self.__address_command, 0xc0)
            sleep(0.001)
        #en:check string length.(max 16 byte in a line) - if string has length over max byte, is written only 16 byte.
        #ja:１行の書き込み可能な長さを超えている場合、１６byteのみ書き込む。オーバー分は破棄する。
        ws = s
        if(len(s) > 16):
            ws = s[0:16]

        #write data
        for c in list(ws):
            byte_data = self.__check_writable(ord(c))
            self.__bus.write_byte_data(self.__address_st7032i, self.__address_data, byte_data)
            #en:write address is auto increased and shifted right.
            #ja:書き込みアドレスは自動的にインクリメントされ右にシフトする。


    def __check_writable(self, c):
        if(c >= 0x06 and c <= 0xff):
            return c
        else:
            return 0x20 #space


    def dispose(self):
        self.__bus.close()
