# -*- coding: utf-8 -*-
#If you want to like japanese mode, add starting parameter 'ja'. 
#For example: python3 hygrothermograph.py ja
import sys
from time import sleep
from haruzirasdk import hz_client_tcp_communication as hzcomm
import bme280
import lcd_st7032i
import hz_voice_control


#main routine of the sample program.

#en:Initialize LCD
#ja:LCDの初期化
lcd = lcd_st7032i.LcdSt7032i()
lcd.init_st7032i()

#en:initialize BME280 senser
#ja:温度・湿度・気圧センサーの初期化
bme = bme280.Bme280()
bme.setup()

#Initialize Haruzira SDK
clt_tcp_comm = hzcomm.ClientTcpCommunication()
hz_vc = hz_voice_control.HzVoiceControl(sys.argv, clt_tcp_comm, bme, lcd)
#start tcp listener
clt_tcp_comm.startAsynchronousListener()

try:
    while True:
        #en:periodic measurement of the temperature, humidity, pressure.(bme280 senser)
        #温度・湿度・気圧の周期的な計測(bme280 senser)
        hz_vc.periodic_measurement()
        sleep(10)   #X seconds interval

except KeyboardInterrupt:
    pass
except Exception as ex:
    print(ex.args)
finally:
    hz_vc.gpio_dispose()
    bme.dispose()
    lcd.dispose()
    #End Haruzira tcp listener. Don't forget.
    clt_tcp_comm.cancelAsynchronousListener()
    print("end program.")
