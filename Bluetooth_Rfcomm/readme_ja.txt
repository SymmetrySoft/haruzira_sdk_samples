**************************************************************
* Bluetooth RFCOMMサービスを利用したサンプルプログラム
**************************************************************

サンプルプログラムを実行する前に、Bluetoothデバイスのペアリングや必要なモジュールのインストールなどを事前に
済ませて下さい。
詳細な方法については、次のURLを参照して下さい。
http://haruzirasdk.wpblog.jp/usage/bluetooth-rfcommremote-operation/

１．サンプルプログラムファイルの説明
	・bt_console_sample.py：
		電子回路を利用しないコンソールプログラム。コンソール上のメニューから操作できます。
		日本語メニューを表示したい場合は、起動時に「ja」オプションを付加して下さい。
		例）python3 bt_console_sample.py ja

		起動前にファイル内の44、45行目を、ユーザーの環境に合わせて変更して下さい。
		port = 1                        #Your bluetooth channel (server - working for Haruzira)
		host = "XX:XX:XX:XX:XX:XX"      #Your bluetooth device address (server - working for Haruzira)

	
	・bt_rfcomm_sample.py：　サンプルの電子回路を利用したプログラム。電子回路上で操作できます。
		配線図や回路図は、次のURLを参照して下さい。
		http://haruzirasdk.wpblog.jp/usage/bluetooth-rfcommremote-operation/sample-programs/
	
		起動前にファイル内の22、23行目を、ユーザーの環境に合わせて変更して下さい。
		port = 1                        #Your bluetooth channel (server - working for Haruzira)
		host = "XX:XX:XX:XX:XX:XX"      #Your bluetooth device address (server - working for Haruzira)
	


２．音声認識などを利用しリモート通信でコマンドを送信する場合は、Haruziraで事前に以下の設定を行って下さい。
	①リモートデバイス管理ページで送信先のデバイス情報を登録する
	②リモート音声コマンド管理ページでコマンド情報の登録を行う。
	※各管理ページで、サンプルで利用した上記データを以下のファイルから取り込むことができます。
	  (デバイス情報は、利用者の環境に合わせて変更して下さい。)
	　- リモートデバイス情報ファイル：HzClientDevicesDemo.json
	　- リモート音声コマンド情報ファイル：HzRemoteVoiceCommandsDemo.json




