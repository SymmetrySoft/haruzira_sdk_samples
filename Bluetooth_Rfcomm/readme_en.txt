**************************************************************
* Sample programs for using bluetooth rfcomm service
**************************************************************

When you execute sample program, need to set up of that installing some packages, pairing bluetooth device, 
and more in advance.
Please refer to following url.
http://haruzirasdke.wpblog.jp/usage/bluetooth-rfcommremote-operation/

1. Description of sample program files.
	・bt_console_sample.py
		This sample program is operated on console.(not use electronics circuit)
		If you want to display Japanese menu, can add execute option.
		ex.)python3 bt_console_sample.py ja

		You must to modify line numbers of 44, 45 in this file for your environment before startup.
		port = 1                        #Your bluetooth channel (server - working for Haruzira)
		host = "XX:XX:XX:XX:XX:XX"      #Your bluetooth device address (server - working for Haruzira)


	・bt_rfcomm_sample.py
		This sample program is operated by electronics circuit.
		Please refer to following url about a wiring diagram and a circuit diagram.
		http://haruzirasdke.wpblog.jp/usage/bluetooth-rfcommremote-operation/bt-rfcomm-sample-programs/

		You must to modify line numbers of 22, 23 in this file for your environment before startup.
		port = 1                        #Your bluetooth channel (server - working for Haruzira)
		host = "XX:XX:XX:XX:XX:XX"      #Your bluetooth device address (server - working for Haruzira)


2. When you will use voice recognition on the network, need to set up of the following in advance using Haruzira.
	a) Remote Devices Page: Add devices informations.
	b) Remote Voice Commands Page: Add commands informations.
	
	*You can import the following sample data file at each page.
	 - Remote Devices data: HzClientDevicesDemo.json
	 - Remote Voice Commands data: HzRemoteVoiceCommandsDemo.json
	 (note: you need to edit of the devices data, match in your environment.)


