# coding: utf-8
import threading
import sys
import time
import bluetooth

#menu of english
menu_en = '''
=============================================================================
    Menu for demo
=============================================================================
[0]:Start voice recognition
[1]:Send user command
[2]:Lock remote oprations
[99]:quit(exit)
=============================================================================

Enter a number：
'''

#menu of japanese
menu_ja = '''
=============================================================================
    デモ用メニュー
=============================================================================
[0]:音声認識の開始
[1]:ユーザーコマンド送信
[2]:リモート操作のロック
[99]:終了(QUIT)
=============================================================================

[番号]を入力して下さい：
'''

##Usage Haruzira command = "00:" + "command name"
START_COMMAND = "00:" + "Start voice recognition"
LOCK_COMMAND = "00:" + "Lock remote operation"
##Usage User command = "01:" + "command name"
USER_COMMAND = "01:" + "Weather condition"
#USER_COMMAND2 = "01:" + "Alert On"

th_rcv = None    #thread object for receiving
sock = None
port = 1                        #Your bluetooth channel (server - working for Haruzira)
host = "XX:XX:XX:XX:XX:XX"      #Your bluetooth device address (server - working for Haruzira)
name = "Haruzira bluetooth rfcomm service."
is_connected = False        #True:connected, False:disconnected


# en:get a parameter to set language code
# ja:言語設定を行うためのパラメータ取得
#strings = strings_en
argvs = sys.argv
lang = ""
if(len(argvs) > 1):
    lang = argvs[1].lower()
    if(lang == "ja"):
        menu = menu_ja
        #strings = strings_ja
    else:
        menu = menu_en
else:
    menu = menu_en


###############################################################
# en:Command sending functions.
# ja:コマンド送信関数定義
###############################################################
#ja:音声認識開始コマンド送信
def start_voice_recognition():
    if(connect_server()):
        sock.send(START_COMMAND)
        print(START_COMMAND)


#ja:リモート操作ロックコマンド送信
def lock_remote_operation():
    if(connect_server()):
        sock.send(LOCK_COMMAND)
        print(LOCK_COMMAND)

#ja:ユーザーコマンド送信 - weather condition
def send_user_command():
    if(connect_server()):
        sock.send(USER_COMMAND)
        print(USER_COMMAND)


#Connect to server
def connect_server():
    global sock
    global is_connected
    try:
        #connect to server
        if(is_connected == False):
            sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
            sock.connect((host, port))
            is_connected = True
            print("connect success")
        return True
    except Exception as ex:
        print(ex.args)
        return False
    

print("start connecting to \"%s\" on %s port %d" % (name, host, port))

def rcv_worker():
    global sock
    global is_connected
    try:
        stop_event_rcv.clear()

        while True:
            try:

                while True:
                    if(is_connected != True):
                        break
                    data = sock.recv(1024)
                    if(len(data) == 0):
                        print("no data")
                        raise Exception("disconnected by server.")
                        #time.sleep(1)
                    else:
                        print(data)
                        msg = data.decode("utf-8")
                        if(msg == "00:disconnect"):
                            raise Exception("disconnected by server.")
                        elif(msg == "quit"):
                            raise Exception("stop receiving thread.")
                        
            except Exception as ex:
                print(ex.args)
                if(sock is not None):
                    sock.close()
                    sock = None
                if(stop_event_rcv.is_set()):
                    break
                #time.sleep(3)
            finally:
                is_connected = False
                time.sleep(3)
    except Exception as ex:
        print(ex.args)
    finally:
        #time.sleep(1)
        print("\rEnd receiving thread.")


###############################################################
# en:start main proccesing
# ja:メイン処理開始
###############################################################
th_rcv = None    #thread object for receiving
stop_event_rcv = threading.Event()
print(menu)
try:
    th_rcv = threading.Thread(target=rcv_worker)
    th_rcv.daemon = True
    th_rcv.start()

    while True:
        s = input().strip()
        try:
            num = -1 if s == "" else int(s)
        except Exception as ex:
            num = -2

        if(num == 0):
            #Start voice recognition
            start_voice_recognition() 
        elif(num == 1):
            #Send user command
            send_user_command()
        elif(num == 2):
            #Lock remote operations
            lock_remote_operation()
        elif(num == 99):
            print("completed demo.")
            if(th_rcv != None):
                # en:Stop a receiving thread.
                # ja:受信スレッド終了
                stop_event_rcv.set()
                #sys.exit(0)
                #th_rcv.join(5)
            break 
        elif(num == -1):
            pass
        else:
            print("Invalid number selected.")

        print(menu)

except KeyboardInterrupt:
    pass
except Exception as ex:
    print(ex.args)
finally:
    #time.sleep(1)
    print("\rdisconnected.")
    if(sock is not None):
        sock.close()
        sock = None


