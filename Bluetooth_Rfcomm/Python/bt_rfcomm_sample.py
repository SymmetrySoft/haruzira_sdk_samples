# coding: utf-8
import threading
import sys
import time
import bluetooth
import RPi.GPIO as GPIO


GPIO_TACTSW_VOICE = 17      #Tact switch for voice recognition starting command
GPIO_TACTSW_LOCK = 27       #Tact switch for lock command
GPIO_TACTSW_USER = 24       #Tact switch for user command
GPIO_LED_GREEN = 23         #LED(green)

#Haruzira command = "00:" + "command name"
START_COMMAND = "00:" + "Start voice recognition"
LOCK_COMMAND = "00:" + "Lock remote operation"
#User command = "01:" + "command name"
USER_COMMAND = "01:" + "Weather condition"
#USER_COMMAND2 = "01:" + "Alert On"

sock = None
port = 1                        #Your bluetooth channel (server - working for Haruzira)
host = "XX:XX:XX:XX:XX:XX"      #Your bluetooth device address (server - working for Haruzira)
name = "Haruzira bluetooth rfcomm service."
is_connected = False        #True:connected, False:disconnected

###############################################################
# en:call back functions. called when occurred events on GPIO.
# ja:GPIOイベントコールバック関数定義
###############################################################
#en:Tact switch event (voice recognition starting command)
#ja:タクトスイッチのイベント検出用（音声認識開始コマンド送信）
def start_voice_recognition_callback(ch):
    if(ch == GPIO_TACTSW_VOICE and connect_server() == True):
        sock.send(START_COMMAND)
        print(START_COMMAND)


#en:Tact switch event (remote operation locking command)
#ja:タクトスイッチのイベント検出用（リモート操作ロックコマンド送信）
def lock_remote_operation_callback(ch):
    if(ch == GPIO_TACTSW_LOCK and connect_server() == True):
        sock.send(LOCK_COMMAND)
        print(LOCK_COMMAND)

#en:Tact switch event (your user command sending command)
#ja:タクトスイッチのイベント検出用（ユーザーコマンド - weather condition）
def send_user_command_callback(ch):
    if(ch == GPIO_TACTSW_USER and connect_server() == True):
        sock.send(USER_COMMAND)
        print(USER_COMMAND)


#Connect to server
def connect_server():
    global sock
    global is_connected
    try:
        #connect to server
        if(is_connected == False):
            sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
            sock.connect((host, port))
            is_connected = True
            GPIO.output(GPIO_LED_GREEN, GPIO.HIGH)
            print("connect success")
        return True
    except Exception as ex:
        print(ex.args)
        return False
    


GPIO.setwarnings(False)
try:
    GPIO.cleanup()
except Exception as ex:
    print(ex.args)


#Initialize GPIO. 
GPIO.setmode(GPIO.BCM) #BCM:GPIO No, BOARD:BOARD No
GPIO.setup(GPIO_TACTSW_VOICE, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(GPIO_TACTSW_LOCK, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(GPIO_TACTSW_USER, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(GPIO_LED_GREEN, GPIO.OUT, initial=GPIO.LOW)
#Register callback function.(occurred when detected rising event)
#bouncetime is needed for clearing noise of positive edge event.
GPIO.add_event_detect(GPIO_TACTSW_VOICE, GPIO.RISING, callback=start_voice_recognition_callback, bouncetime=2000)
GPIO.add_event_detect(GPIO_TACTSW_LOCK, GPIO.RISING, callback=lock_remote_operation_callback, bouncetime=2000)
GPIO.add_event_detect(GPIO_TACTSW_USER, GPIO.RISING, callback=send_user_command_callback, bouncetime=2000)



print("start bluetooth rfcomm service to \"%s\" on %s port %d" % (name, host, port))

try:

    while True:
        try:

            while True:
                if(is_connected != True):
                    break
                data = sock.recv(1024)
                if(len(data) == 0):
                    print("no data")
                    raise Exception("disconnected by server.")
                    #time.sleep(1)
                else:
                    print(data)
                    msg = data.decode("utf-8")
                    if(msg == "00:disconnect"):
                        raise Exception("disconnected by server.")
        except Exception as ex:
            print(ex.args)
            if(sock is not None):
                sock.close()
                sock = None
                GPIO.output(GPIO_LED_GREEN, GPIO.LOW)
            #time.sleep(3)
        finally:
            is_connected = False
            time.sleep(3)
except KeyboardInterrupt:
    pass
except Exception as ex:
    print(ex.args)
finally:
    #time.sleep(1)
    print("\rdisconnected.")
    GPIO.cleanup()
    if(sock is not None):
        sock.close()
        sock = None


