#!/usr/bin/ruby
# -*- coding: utf-8 -*-
#This program will be ended when enter 'Ctrl + C'.
require "timeout"
require "haruzira_sdk"

class LedControlWithVoice
    GPIO_25 = 25
    GPIO_24 = 24
    @@strings_en = {:led_on => "I have accepted your command, and done it.", \
                :led_off => "Yes, certainly.\r\nI will do it right now.", \
                :invalid_command => "Invalid command data received. \r\nYour command '%s' can not any work." \
                }
    @@strings_ja = {:led_on => "お疲れ様です。\r\nLEDを点灯しました。", \
                :led_off => "おやすみなさい。\r\n今からLEDを消灯します。", \
                :invalid_command => "不明なコマンドを受信しました。コマンド「{}」に対する処理は実行できません。" \
                }
    @@langid_tbl = {:en => 0x0409, :ja => 0x0411}
    @@strings = @@strings_en
    @@langid = @@langid_tbl[:en]

    def initialize(ip, port, trace)
        @clt_tcp_comm = ClientTcpCommunication.new
        @clt_tcp_comm.ServerPortNo = port #port number for access point
        @clt_tcp_comm.ServerIP = ip #ip address for access point
        @clt_tcp_comm.setTraceOutPut(trace)
        @ledOff = false
        @work_tact = nil
        @tact_state = false 
        
        # en:get a parameter to set language code
        # ja:言語設定を行うためのパラメータ取得
        lang = ARGV[0].to_s.downcase 
        if lang == "ja"
            @@strings = @@strings_ja
            @@langid = @@langid_tbl[:ja]
        else
            @@strings = @@strings_en
            @@langid = @@langid_tbl[:en]
        end

        ###############################################################
        # en:call back functions for haruzirasdk. called when occurred events on a network communication.
        # ja:通信イベントコールバック関数定義
        ###############################################################
        # <summary>
        # en:notify when occurred issue that disconnected from access point or on receiving threads.
        # ja:切断発生時通知イベント（サーバー側からの切断または受信スレッドで異常発生時）
        # </summary>
        # <param name="msg">message</param>
        # <param name="st">status</param>
        @clt_tcp_comm.EvNotifyReceivedDisConnectEvent = lambda{|msg, st|
            puts("{%s}, Status[0x%02x]" % [msg, st])
        }

        # <summary>
        # en:notify when occurred issue for some reasons(except disconnect).
        # ja:通信障害（切断以外）発生時通知イベント
        # </summary>
        # <param name="msg">message</param>
        # <param name="msg_id">message id</param>
        # <param name="err_code">error code</param>
        @clt_tcp_comm.EvNotifyMessageEvent = lambda{|msg, msg_id, err_code|
            puts("notifyCommunicatonMessage Message ID[0x%02x], Error Code[0x%02x], %s" % [msg_id, err_code, msg])
        }

        # <summary>
        # en:notify when received a 'speech completion notice' message.
        # ja:読み上げ完了通知受信時通知イベント
        # </summary>
        # <param name="result">submit result</param>
        # <param name="time_stamp">time stamp</param>
        @clt_tcp_comm.EvNotifyCompeteSpeech = lambda{|result, time_stamp|
            puts("recieved notify complete speech. time stamp[%s]" % [time_stamp])
            if(@ledOff)
                #light off
                File.write("/sys/class/gpio/gpio#{GPIO_25}/value", 0)
                @tact_state = false
            end
        }

        # <summary>
        # en:notify when receive a 'send speech recognition command' message.
        # ja:音声認識コマンド送信メッセージ受信時イベント
        # </summary>
        # <param name="cmdInfo">HzSpeechRecognitionCommandInfo class object</param>
        @clt_tcp_comm.EvNotifySendSpeechRecognitionCommand = lambda{|cmdInfo|
            puts("Received, 'Send Speech Recognition Command' message. : Ip[%s], Port[%d], Mode[%d], Command[%s], Timestamp[%s]" % [cmdInfo.IpAddr, cmdInfo.Port, cmdInfo.Mode, cmdInfo.Command, cmdInfo.Timestamp])
            @ledOff = false
            if (cmdInfo.Command.casecmp("led light on") == 0)
                msg = @@strings[:led_on]
                File.write("/sys/class/gpio/gpio#{GPIO_25}/value", 1)
                @tact_state = true
            elsif (cmdInfo.Command.casecmp("led light off") == 0)
                msg = @@strings[:led_off]
                @ledOff = true
            else
                msg = @@strings[:invalid_command] % [cmdInfo.Command]
            end
            #send speech data.
            @clt_tcp_comm.ServerPortNo = cmdInfo.Port
            @clt_tcp_comm.ServerIP = cmdInfo.IpAddr
            @clt_tcp_comm.ReqSendDataText = msg
            @clt_tcp_comm.ReqSendDataSpeechLocaleId = @@langid
            #@clt_tcp_comm.ReqSendDataSpeechGender = HzSpeechGender::Male
            timestamp = @clt_tcp_comm.sendSpeechDataEx()
            if(timestamp != nil)
                puts("success return message. timestamp:#{timestamp}")
            end
        }
        #end define call back function 
    end

    # <summary>
    # en:monitoring thread for tact switch state.
    # ja:タクトスイッチ監視スレッド
    # </summary>
    def tactswMonitoringWorker()
        Thread.abort_on_exception = true
        begin
            @work_tact = Thread.start do   
                puts("start monitoring thread for tact switch state.")
                loop do
                    if(File.read("/sys/class/gpio/gpio#{GPIO_24}/value").to_i == 1)
                        if(@tact_state == false)
                            #light on
                            File.write("/sys/class/gpio/gpio#{GPIO_25}/value", 1)
                            @tact_state = true
                        else
                            #light off
                            File.write("/sys/class/gpio/gpio#{GPIO_25}/value", 0)
                            @tact_state = false
                        end
                        #en:bouncetime is needed for clearing noise of positive edge event.
                        #ja:ポジティブ・エッジイベントのノイズをクリアするためにスリープ.
                        sleep 0.5
                    end
                    sleep 0.05
                end
            end
        rescue Exception => ex
            puts(ex)
        ensure
        end
    end

    # <summary>
    # en:start LED blinking test.
    # ja:LED点滅処理
    # </summary>
    # <param name="tm">sleep time for loop.</param>
    def startLedControl(tm)
        @clt_tcp_comm.startAsynchronousListener()
        begin
            File.write("/sys/class/gpio/export", GPIO_25)
            File.write("/sys/class/gpio/export", GPIO_24)
        rescue Exception
            #if the gpio is in busy.
            File.write("/sys/class/gpio/unexport", GPIO_25)
            File.write("/sys/class/gpio/export", GPIO_25)
            File.write("/sys/class/gpio/unexport", GPIO_24)
            File.write("/sys/class/gpio/export", GPIO_24)
        ensure
            sleep 0.5   #protection of time lag for when changing GPIO permission.
            File.write("/sys/class/gpio/gpio#{GPIO_25}/direction", "out")
            File.write("/sys/class/gpio/gpio#{GPIO_24}/direction", "in")
        end

        #start monitoring thread for task switch state.
        tactswMonitoringWorker

        #Loop until when be interrupted by 'Ctrl + C'.
        begin
            while true
                sleep tm
            end
        rescue Exception => ex
            puts(ex)
        ensure
            File.write("/sys/class/gpio/unexport", GPIO_25)
            File.write("/sys/class/gpio/unexport", GPIO_24)
            if(@work_tact != nil)
                @work_tact.kill
            end
        end 
    end
end

#========== main routine ===================
led = LedControlWithVoice.new("192.168.1.2", 46000, false)
led.startLedControl(0.05)

