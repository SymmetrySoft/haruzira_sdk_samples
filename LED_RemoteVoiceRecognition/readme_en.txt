1. You can be able to work of the following features using this sample program.
	a) LED ON/OFF by tact switch.
	b) LED ON/OFF by voice recognition on the network.
	*When sample program startup, if you add 'ja' option, can transfer japanese text data.
	(you need to install japanese language pack on environment of Haruzira working device using windows 10 settings in this case.)


2. When you will use voice recognition on the network, need to set up of the following in advance using Haruzira.
	a) Remote Devices Page: Add devices informations.
	b) Remote Voice Commands Page: Add commands informations.
	
	*You can take in following sample data file at each page.(It was used on demo video)
	 - Remote Devices data: HzClientDevicesDemo.json
	 - Remote Voice Commands data: HzRemoteVoiceCommandsDemo.json
	 (note: you need to edit of the devices data, match in your environment.)


Also, please refer to SDK user's manual too.
