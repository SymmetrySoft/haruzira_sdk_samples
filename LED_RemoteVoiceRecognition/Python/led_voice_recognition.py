# coding: utf-8
#This program will be ended when enter 'Ctrl + C'.
import RPi.GPIO as GPIO
import sys
import time
import threading
from haruzirasdk import hz_client_tcp_communication as hzcomm

strings_en = {"led_on":"I have accepted your command, and done it.", \
              "led_off":"Yes, certainly.\r\nI will do it right now.", \
              "invalid_command":"Invalid command data received.\r\nYour command '{}' can not any work." \
             }
strings_ja = {"led_on":"お疲れ様です。\r\nLEDを点灯しました。", \
              "led_off":"おやすみなさい。\r\n今からLEDを消灯します。", \
              "invalid_command":"不明なコマンドを受信しました。コマンド「{}」に対する処理は実行できません。" \
             }
langid_tbl = {"en":0x0409, "ja":0x0411}
led_off = False

###############################################################
# en:call back functions for haruzirasdk. called when occurred events on a network communication.
# ja:通信イベントコールバック関数定義
###############################################################
# <summary>
# en:notify when occurred issue that disconnected from access point or on receiving threads.
# ja:切断発生時通知イベント（サーバー側からの切断または受信スレッドで異常発生時）
# </summary>
# <param name="msg">message</param>
# <param name="st">status</param>
def notify_received_disconnect(msg, st):
    print("{0:s}, Status[0x{1:02x}]".format(msg, st))

# <summary>
# en:notify when occurred issue for some reasons(except disconnect).
# ja:通信障害（切断以外）発生時通知イベント
# </summary>
# <param name="msg">message</param>
# <param name="msg_id">message id</param>
# <param name="err_code">error code</param>
def notify_communicaton_message(msg, msg_id, err_code):
    print("Message ID[0x{0:02x}], Error Code[0x{1:02x}], {2:s}".format(msg_id, err_code, msg))

# <summary>
# en:notify when received a 'speech completion notice' message.
# ja:読み上げ完了通知受信時通知イベント
# </summary>
# <param name="result">submit result</param>
# <param name="time_stamp">time stamp</param>
def rcv_notify_complete_speech(result, time_stamp):
    global led_state
    #print("{0:s}：result[0x{1:02x}], time stamp[{2:s}]".format(strings["rec_comp"], result, time_stamp))
    print("recieved notify complete speech. time stamp[{0:s}]".format(time_stamp))
    if(led_off):
        #light off
        GPIO.output(25, GPIO.LOW)
        led_state = GPIO.LOW

# <summary>
# en:notify when receive a 'send speech recognition command' message.
# ja:音声認識コマンド送信メッセージ受信時イベント
# </summary>
# <param name="cmdInfo">HzSpeechRecognitionCommandInfo class object</param>
def rcv_send_speech_recognition_command(cmdInfo):
    try:
        print("{0:s} : Ip[{1:s}], Port[{2:d}], Mode[{3:d}], Command[{4:s}], Timestamp[{5:s}]".format("Received, 'Send Speech Recognition Command' message.", 
            cmdInfo.ip_addr, cmdInfo.port, cmdInfo.mode, cmdInfo.command, cmdInfo.timestamp))
        global led_state
        global led_off
        led_off = False
        #comp_evt.clear()
        if(cmdInfo.command.lower() == "led light on"):
            msg = strings["led_on"]
            GPIO.output(25, GPIO.HIGH)
            led_state = GPIO.HIGH
        elif(cmdInfo.command.lower() == "led light off"):
            msg = strings["led_off"]
            led_off = True
        else:
            msg = strings["invalid_command"].format(cmdInfo.command)
        
        #return a message to sender.
        clt_tcp_comm.ServerPortNo = cmdInfo.port
        clt_tcp_comm.ServerIP = cmdInfo.ip_addr
        clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Male.value
        clt_tcp_comm.ReqSendDataText = msg
        clt_tcp_comm.sendSpeechDataEx()
    except Exception as ex:
        print(ex.args)
#end define call back function for haruzira


#en:callback function.(occurred when detected rising event)
#This function is used by tact switch. (It's not used by 'haruzira sdk')
#ja:タクトスイッチのイベント検出用（タクトスイッチを利用しない場合は不要）
def rising_callback(channel):
    global led_state
    if channel == 24:
        led_state = not led_state
        if led_state == GPIO.HIGH:
            GPIO.output(25, GPIO.HIGH)
        else:
            GPIO.output(25, GPIO.LOW)


# en:get a parameter to set language code
# ja:言語設定を行うためのパラメータ取得
strings = strings_en
langid = langid_tbl["en"]
argvs = sys.argv
lang = ""
if(len(argvs) > 1):
    lang = argvs[1].lower()
    if(lang == "ja"):
        strings = strings_ja
        langid = langid_tbl["ja"]
    else:
        strings = strings_en
        langid = langid_tbl["en"]
else:
    strings = strings_en
    langid = langid_tbl["en"]

#Initialize haruzira sdk class. and register callback function.
clt_tcp_comm = hzcomm.ClientTcpCommunication()
clt_tcp_comm.ServerPortNo = 46000 #port number for access point
clt_tcp_comm.ServerIP = "192.168.1.6" #ip address for access point
clt_tcp_comm.evNotifyCompleteSpeech = rcv_notify_complete_speech
clt_tcp_comm.evNotifyMessageEvent = notify_communicaton_message
clt_tcp_comm.evNotifyReceivedDisconnectEvent = notify_received_disconnect
clt_tcp_comm.evNotifySendSpeechRecognitionCommand = rcv_send_speech_recognition_command
clt_tcp_comm.setTraceOutPut(False)
#comp_evt = threading.Event()
#start tcp listener
clt_tcp_comm.startAsynchronousListener()


#Initialize GPIO. 
GPIO.setmode(GPIO.BCM) #BCM:GPIO No, BOARD:BOARD No
GPIO.setup(25, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(24, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) #It use pull-down resistor on the raspberry pi.
#Register callback function.(occurred when detected rising event)
#bouncetime is needed for clearing noise of positive edge event.
GPIO.add_event_detect(24, GPIO.RISING, callback=rising_callback, bouncetime=500)

led_state = GPIO.LOW

#Loop until when be interrupted by 'Ctrl + C'.
try:
    while True:
        time.sleep(0.03)

except Exception as ex:
    print(ex.args)
finally:
    GPIO.cleanup()
    #End Haruzira tcp listener. Don't forget.
    clt_tcp_comm.cancelAsynchronousListener()
