# coding: utf-8
import RPi.GPIO as GPIO
import time
import threading
from haruzirasdk import hz_client_tcp_communication as hzcomm

start_msgs = ("Hi, I'm Haruzira. I will start blinking test of LED with some synthesis voices by remote access.",
              "Bonjour, je suis Haruzira. Je vais commencer à clignoter test de LED avec quelques voix de synthèse par accès à distance.",
                "こんにちは、私はハルジラです。 今からリモートアクセスで音声合成を利用したLEDの点滅テストを開始します。")
end_msgs = ("It's finished. Thank you for watching my video.",
            "C'est fini. Merci de regarder ma vidéo.",
                "これで終了です。最後まで動画を観ていただきありがとうございました。")
langid_tbl = (0x0409, 0x040c, 0x0411)
startgender_tbl = ( hzcomm.HzSpeechGender.Female,  hzcomm.HzSpeechGender.Female,  hzcomm.HzSpeechGender.Male)
endgender_tbl = ( hzcomm.HzSpeechGender.Male,  hzcomm.HzSpeechGender.Male,  hzcomm.HzSpeechGender.Female)
comp_evt = threading.Event()

###############################################################
# en:call back functions for haruzirasdk. called when occurred events on a network communication.
# ja:通信イベントコールバック関数定義
###############################################################
# <summary>
# en:notify when occurred issue that disconnected from access point or on receiving threads.
# ja:切断発生時通知イベント（サーバー側からの切断または受信スレッドで異常発生時）
# </summary>
# <param name="msg">message</param>
# <param name="st">status</param>
def notify_received_disconnect(msg, st):
    print("{0:s}, Status[0x{1:02x}]".format(msg, st))

# <summary>
# en:notify when occurred issue for some reasons(except disconnect).
# ja:通信障害（切断以外）発生時通知イベント
# </summary>
# <param name="msg">message</param>
# <param name="msg_id">message id</param>
# <param name="err_code">error code</param>
def notify_communicaton_message(msg, msg_id, err_code):
    print("Message ID[0x{0:02x}], Error Code[0x{1:02x}], {2:s}".format(msg_id, err_code, msg))

# <summary>
# en:notify when received a 'speech completion notice' message.
# ja:読み上げ完了通知受信時通知イベント
# </summary>
# <param name="result">submit result</param>
# <param name="time_stamp">time stamp</param>
def rcv_notify_complete_speech(result, time_stamp):
    #print("{0:s}：result[0x{1:02x}], time stamp[{2:s}]".format(strings["rec_comp"], result, time_stamp))
    print("recieved notify complete speech. time stamp[{0:s}]".format(time_stamp))
    comp_evt.set()
#end define call back function 


# <summary>
# en:send messages when test start or test end.
# ja:開始・終了時メッセージの音声読み上げテキスト送信
# </summary>
# <param name="msgtbl">message table object</param>
# <param name="gendertbl">gender table object</param>
def text2speech(msgtbl, gendertbl):
    for i in range(0, len(langid_tbl)):
        comp_evt.clear()
        clt_tcp_comm.ReqSendDataText = msgtbl[i]
        clt_tcp_comm.ReqSendDataSpeechLocaleId = langid_tbl[i]
        clt_tcp_comm.ReqSendDataSpeechGender = gendertbl[i].value
        timestamp = clt_tcp_comm.sendSpeechDataEx()
        if(timestamp is not None):
            print("time stamp[{0:s}]".format(timestamp))
        comp_evt.wait(20)

#Initialize haruzira sdk.
clt_tcp_comm = hzcomm.ClientTcpCommunication()
clt_tcp_comm.ServerPortNo = 46000 #port number for access point
clt_tcp_comm.ServerIP = "192.168.1.2" #ip address for access point
clt_tcp_comm.evNotifyCompleteSpeech = rcv_notify_complete_speech
clt_tcp_comm.evNotifyMessageEvent = notify_communicaton_message
clt_tcp_comm.evNotifyReceivedDisconnectEvent = notify_received_disconnect
clt_tcp_comm.setTraceOutPut(False)

#send starting messages
text2speech(start_msgs, startgender_tbl)

#Initialize GPIO
GPIO.setmode(GPIO.BCM)  #BCM:GPIO No, BOARD:BOARD No
GPIO.setup(14, GPIO.OUT, initial=GPIO.LOW)
count = 0

try:
    #5 times blink
    while count < 5:
        GPIO.output(14, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(14, GPIO.LOW)
        #Send count up message.
        comp_evt.clear()
        clt_tcp_comm.ReqSendDataText = str(count + 1)
        clt_tcp_comm.ReqSendDataSpeechLocaleId = 0x0409
        clt_tcp_comm.ReqSendDataSpeechGender = hzcomm.HzSpeechGender.Male.value
        timestamp = clt_tcp_comm.sendSpeechDataEx()
        if(timestamp is not None):
            print("time stamp[{0:s}]".format(timestamp))
        else:
            #wait for receiving 'Speech Completion Notice' message.
            comp_evt.wait(5)
        count = count + 1
except Exception as ex:
    pass
finally:
    GPIO.cleanup()

#send ending messages
text2speech(end_msgs, endgender_tbl)
#Haruzira listener close
clt_tcp_comm.cancelAsynchronousListener()
