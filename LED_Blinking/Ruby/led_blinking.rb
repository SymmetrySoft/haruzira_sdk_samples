#!/usr/bin/ruby
# -*- coding: utf-8 -*-
require "timeout"
require "haruzira_sdk"

class LedBlinkWithTts
    GPIO_14 = 14
    @@start_msgs = ["Hi, I'm Haruzira. I will start blinking test of LED with some synthesis voices by remote access.",
                "Bonjour, je suis Haruzira. Je vais commencer à clignoter test de LED avec quelques voix de synthèse par accès à distance.",
                    "こんにちは、私はハルジラです。 今からリモートアクセスで音声合成を利用したLEDの点滅テストを開始します。"]
    @@end_msgs = ["It's finished. Thank you for watching my video.",
                "C'est fini. Merci de regarder ma vidéo.",
                    "これで終了です。最後まで動画を観ていただきありがとうございました。"]
    @@langid_tbl = [0x0409, 0x040c, 0x0411]
    @@startgender_tbl = [HzSpeechGender::Female, HzSpeechGender::Female, HzSpeechGender::Male]
    @@endgender_tbl = [HzSpeechGender::Male, HzSpeechGender::Male, HzSpeechGender::Female]

    def initialize(ip, port, trace)
        @clt_tcp_comm = ClientTcpCommunication.new
        @clt_tcp_comm.ServerPortNo = port #port number for access point
        @clt_tcp_comm.ServerIP = ip #ip address for access point
        @clt_tcp_comm.setTraceOutPut(trace)
        @queWaitNotifyCompleteSpeech = Queue.new 
        ###############################################################
        # en:call back functions for haruzirasdk. called when occurred events on a network communication.
        # ja:通信イベントコールバック関数定義
        ###############################################################
        # <summary>
        # en:notify when occurred issue that disconnected from access point or on receiving threads.
        # ja:切断発生時通知イベント（サーバー側からの切断または受信スレッドで異常発生時）
        # </summary>
        # <param name="msg">message</param>
        # <param name="st">status</param>
        @clt_tcp_comm.EvNotifyReceivedDisConnectEvent = lambda{|msg, st|
            puts("{%s}, Status[0x%02x]" % [msg, st])
        }

        # <summary>
        # en:notify when occurred issue for some reasons(except disconnect).
        # ja:通信障害（切断以外）発生時通知イベント
        # </summary>
        # <param name="msg">message</param>
        # <param name="msg_id">message id</param>
        # <param name="err_code">error code</param>
        @clt_tcp_comm.EvNotifyMessageEvent = lambda{|msg, msg_id, err_code|
            puts("notifyCommunicatonMessage Message ID[0x%02x], Error Code[0x%02x], %s" % [msg_id, err_code, msg])
        }

        # <summary>
        # en:notify when received a 'speech completion notice' message.
        # ja:読み上げ完了通知受信時通知イベント
        # </summary>
        # <param name="result">submit result</param>
        # <param name="time_stamp">time stamp</param>
        @clt_tcp_comm.EvNotifyCompeteSpeech = lambda{|result, time_stamp|
            puts("recieved notify complete speech. time stamp[%s]" % [time_stamp])
            @queWaitNotifyCompleteSpeech.push("OK")
        }
        #end define call back function 
    end


    # <summary>
    # en:waiting for response within timeout limit.
    # ja:応答待機処理(タイムアウト制限有り)
    # </summary>
    # <param name="t_val">timeout value to wait for response.</param>
    # <param name="que">message queue object</param>
    def queuePopWithTimeout(t_val, que)
        ret = false
        begin
            Timeout.timeout(t_val) do
                qret = que.pop
            end
            ret = true
        rescue Timeout::Error => ex
            puts("応答待ちのタイムアウトが発生しました。")
            ret = false
        ensure
            return ret
        end 
    end

    # <summary>
    # en:send messages when test start or test end.
    # ja:開始・終了時メッセージの音声読み上げテキスト送信
    # </summary>
    # <param name="msgtbl">message table object</param>
    # <param name="gendertbl">gender table object</param>
    def text2speech(msgtbl, gendertbl)
        for i in 0..(@@langid_tbl.length - 1) do
            @queWaitNotifyCompleteSpeech.clear
            @clt_tcp_comm.ReqSendDataText = msgtbl[i]
            @clt_tcp_comm.ReqSendDataSpeechLocaleId = @@langid_tbl[i]
            @clt_tcp_comm.ReqSendDataSpeechGender = gendertbl[i]
            timestamp = @clt_tcp_comm.sendSpeechDataEx()
            if(timestamp != nil)
                puts("time stamp#{timestamp}")
            end
            queuePopWithTimeout(20, @queWaitNotifyCompleteSpeech)
        end
    end

    # <summary>
    # en:start LED blinking test.
    # ja:LED点滅処理
    # </summary>
    # <param name="tm">blinking times.</param>
    def startLedBlinking(tm)
        #start messages
        text2speech(@@start_msgs, @@startgender_tbl)
        begin
            File.write("/sys/class/gpio/export", GPIO_14)
        rescue Exception
            #if the gpio is in busy.
            File.write("/sys/class/gpio/unexport", GPIO_14)
            File.write("/sys/class/gpio/export", GPIO_14)
        ensure
            sleep 0.5   #protection of time lag for when changing GPIO permission.
            File.write("/sys/class/gpio/gpio#{GPIO_14}/direction", "out")
        end
        for i in 1..tm do
            File.write("/sys/class/gpio/gpio#{GPIO_14}/value", 1)
            sleep 1
            File.write("/sys/class/gpio/gpio#{GPIO_14}/value", 0)
            @queWaitNotifyCompleteSpeech.clear
            @clt_tcp_comm.ReqSendDataText = i.to_s
            @clt_tcp_comm.ReqSendDataSpeechLocaleId = 0x0409
            @clt_tcp_comm.ReqSendDataSpeechGender = HzSpeechGender::Male
            timestamp = @clt_tcp_comm.sendSpeechDataEx()
            if(timestamp != nil)
                puts("time stamp#{timestamp}")
            end
            queuePopWithTimeout(5, @queWaitNotifyCompleteSpeech)
        end

        File.write("/sys/class/gpio/unexport", GPIO_14)

        #end messages
        text2speech(@@end_msgs, @@endgender_tbl)
    end
end

#========== main routine ===================
led = LedBlinkWithTts.new("192.168.1.2", 46000, false)
led.startLedBrinking(5)

